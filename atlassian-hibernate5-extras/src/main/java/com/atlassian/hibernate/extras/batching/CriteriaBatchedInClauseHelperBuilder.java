package com.atlassian.hibernate.extras.batching;

import java.util.function.Predicate;

import com.google.common.base.Preconditions;

/**
 * Convenience builder for {@link CriteriaBatchedInClauseHelper}. By default it sets the query batch size to 1000 and
 * has a predicate which always returns true.
 *
 * @param <S> the type used for the "in" clause
 */
public class CriteriaBatchedInClauseHelperBuilder<S>
{

    protected static final int ORACLE_IN_CLAUSE_LIMIT = 1000;
    private static final Predicate DEFAULT_PREDICATE = o -> true;

    private final Class<S> criteriaClass;
    private String inClauseParameterName;
    protected int batchSize = ORACLE_IN_CLAUSE_LIMIT;
    @SuppressWarnings("unchecked")
    private Predicate<S> predicate = DEFAULT_PREDICATE;

    protected CriteriaBatchedInClauseHelperBuilder(final Class<S> criteriaClass)
    {
        this.criteriaClass = criteriaClass;
    }

    /**
     * Sets the "in" clause parameter name
     *
     * @param parameter the "in" clause parameter name
     * @return this
     */
    public CriteriaBatchedInClauseHelperBuilder<S> withParameter(final String parameter)
    {
        this.inClauseParameterName = parameter;
        return this;
    }

    /**
     * Sets the size of the query batches. Increasing this can increase performance by reducing the amount of queries that
     * will be executed, but care must be taken not to exceed the DBMS' limits. For example in the Oracle database the
     * "in" clause limit is 1000, in MySQL it is limited by the max_packet_size option
     *
     * @param batchSize the size of the query batch
     * @return this
     */
    public CriteriaBatchedInClauseHelperBuilder<S> withBatchSize(final int batchSize)
    {
        this.batchSize = batchSize;
        return this;
    }

    /**
     * Sets the predicate which determines if processing should be continued
     * @param predicate Predicate that visits with every rowset returned from the criteria and decides if processing
*                       should be continued or stopped.
     * @return this
     */
    public CriteriaBatchedInClauseHelperBuilder<S> withPredicate(final Predicate<S> predicate)
    {
        this.predicate = predicate;
        return this;
    }

    /**
     * Builds the {@link CriteriaBatchedInClauseHelper}. The criteriaClass, inClauseParameterName and
     * predicate fields must not be null and batchSize must be larger than 0
     *
     * @return an instance of {@link CriteriaBatchedInClauseHelper}.
     */
    public CriteriaBatchedInClauseHelper<S> build()
    {
        Preconditions.checkNotNull(criteriaClass);
        Preconditions.checkNotNull(inClauseParameterName);
        Preconditions.checkNotNull(predicate);
        Preconditions.checkArgument(batchSize > 0, "Batch size must be larger than 0");
        return new CriteriaBatchedInClauseHelper<>(
                criteriaClass,
                inClauseParameterName,
                batchSize,
                predicate);
    }

    /**
     * Constructs a new builder for the supplied criteria class.
     *
     * @param criteriaClass class for the Hibernate criteria
     * @return a builder instance with the criteria class set
     */
    public static <S> CriteriaBatchedInClauseHelperBuilder<S> of(final Class<S> criteriaClass)
    {
        return new CriteriaBatchedInClauseHelperBuilder<>(criteriaClass);
    }

}
