package com.atlassian.hibernate.extras;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.persister.entity.SingleTableEntityPersister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import static org.springframework.jdbc.support.JdbcUtils.closeResultSet;
import static org.springframework.jdbc.support.JdbcUtils.closeStatement;

/**
 * This class is used to set the hibernate_unique_key.next_hi value to the high bits of the highest id in the DB, +1
 * after an import (for now) to maintain state in the database.
 */
@SuppressWarnings({"deprecation", "unused"})
public class ResetableHiLoGeneratorHelper
{
    public static final String HIBERNATE_UNIQUE_KEY_COLUMN = "next_hi";
    public static final String HIBERNATE_UNIQUE_KEY_TABLE = "hibernate_unique_key";

    private static final Logger log = LoggerFactory.getLogger(ResetableHiLoGeneratorHelper.class);

    private SessionFactory sessionFactory;

    /**
     * TODO delete this method once we have an upgrade process in place
     *
     * @return long the next hi value
     */
    public long getHiValue() throws SQLException
    {
        long result = 0L;

        Connection connection = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try
        {
            Session session = SessionFactoryUtils.getSession(sessionFactory, true);

            connection = session.connection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select " + HIBERNATE_UNIQUE_KEY_COLUMN + " from " + HIBERNATE_UNIQUE_KEY_TABLE);
            if (resultSet.next())
            {
                result = resultSet.getLong(1); // might be null, but that's ok, will return 0;
            }
        }
        catch (SQLException e)
        {
            log.error("Error finding maximum next_hi value", e);
            throw e;
        }
        finally
        {
            closeResultSet(resultSet);
            closeStatement(statement);
            commitConnection(connection);
        }
        return result;
    }

    public void setNextHiValue(List<String> errors)
    {
        Connection connection = null;
        Statement statement = null;
        try
        {
            Session session = SessionFactoryUtils.getSession(sessionFactory, true);
            Map<?, ?> metaDataMap = sessionFactory.getAllClassMetadata();

            connection = session.connection();
            statement = connection.createStatement();

            long maximumId = 0;
            int maxLo = 0;
            for (Object o : metaDataMap.keySet())
            {
                String className = (String) o;

                EntityPersister persister = ((SessionFactoryImplementor) sessionFactory).getEntityPersister(className);
                if (persister instanceof SingleTableEntityPersister &&
                        persister.getIdentifierGenerator() instanceof ResettableTableHiLoGenerator)
                {
                    SingleTableEntityPersister entityPersister = (SingleTableEntityPersister) persister;
                    ResettableTableHiLoGenerator generator = (ResettableTableHiLoGenerator) persister.getIdentifierGenerator();

                    // TODO need to work out why we have this logic.
                    if (maxLo == 0)
                    {
                        maxLo = generator.getMaxLo();
                    }
                    else if (maxLo != generator.getMaxLo())
                    {
                        //throw new UpgradeException("One generator uses " + maxLo + " for maxLo, generator for " +
                        //        c.getName() + " uses " + generator.getMaxLo());
                        errors.add("One generator uses " + maxLo + " for maxLo, generator for " +
                                className + " uses " + generator.getMaxLo());
                    }

                    String[] idColumnNames = entityPersister.getIdentifierColumnNames();
                    if (idColumnNames.length != 1)
                    {
                        errors.add("Expected a single ID column for " + className + " found " + idColumnNames.length);
                    }

                    String sql = "select max(" + idColumnNames[0] + ") from " + entityPersister.getTableName();
                    ResultSet resultSet = null;
                    try
                    {
                        resultSet = statement.executeQuery(sql);
                        if (!resultSet.next())
                        {
                            errors.add("No maximum ID returned for " + className);
                        }
                        long value = resultSet.getLong(1); // might be null, but that's ok, will return 0;
                        if (value > maximumId)
                        {
                            maximumId = value;
                        }
                    }
                    finally
                    {
                        closeResultSet(resultSet);
                    }
                }
            }
            int nextHi = (int) (maximumId / (maxLo + 1)) + 1;
            log.info("Setting new next_hi to " + nextHi);
            if (statement.executeUpdate("update " + HIBERNATE_UNIQUE_KEY_TABLE + " set " + HIBERNATE_UNIQUE_KEY_COLUMN + " = " + nextHi) == 0)
            {
                // no row, need to insert one
                if (statement.executeUpdate("insert into " + HIBERNATE_UNIQUE_KEY_TABLE + " values(" + nextHi + ")") == 0)
                {
                    errors.add("failed to insert initial next_hi value");
                }
            }
        }
        catch (Exception e)
        {
            log.error("Error finding maximum next_hi value", e);
            errors.add(e.getMessage());
        }
        finally
        {
            closeStatement(statement);
            commitConnection(connection);
            log.info("Completed database update: HiLoIdRepairUpgradeTask");
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    private static void commitConnection(Connection connection)
    {
        if (connection != null)
        {
            try
            {
                connection.commit();
            }
            catch (SQLException e)
            {
                // A little late now to do anything
            }
        }
    }
}
