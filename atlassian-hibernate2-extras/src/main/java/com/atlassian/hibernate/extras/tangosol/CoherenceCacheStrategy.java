package com.atlassian.hibernate.extras.tangosol;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.cache.Cache;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.cache.CacheException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Comparator;

/**
 * A Hibernate {@link com.atlassian.hibernate.extras.tangosol.CoherenceCacheStrategy} for distributed caches.
 *
 * Note that despite the name, this class has nothing to do with Coherence, it's a general strategy for distributed caching.
 */
public class CoherenceCacheStrategy implements CacheConcurrencyStrategy
{
    private static final Logger log = LoggerFactory.getLogger(CoherenceCacheStrategy.class);

    private AtlassianHibernateCache cache;
    private int nextLockId;

    public CoherenceCacheStrategy()
    {
    }

    public void setCache(Cache cache)
    {
        if (cache instanceof AtlassianHibernateCache)
        {
            this.cache = (AtlassianHibernateCache) cache;
        }
        else
        {
            throw new IllegalArgumentException(cache.getClass().getName() + " does not implement " + AtlassianHibernateCache.class.getName());
        }
    }

    private synchronized int nextLockId()
    {
        if (nextLockId == Integer.MAX_VALUE) nextLockId = Integer.MIN_VALUE;
        return nextLockId++;
    }

    /**
     * Do not return an item whose timestamp is later than the current
     * transaction timestamp. (Otherwise we might compromise repeatable
     * read unnecessarily.) Do not return an item which is soft-locked.
     * Always go straight to the database instead.<br>
     * <br>
     * Note that since reading an item from that cache does not actually
     * go to the database, it is possible to see a kind of phantom read
     * due to the underlying row being updated after we have read it
     * from the cache. This would not be possible in a lock-based
     * implementation of repeatable read isolation. It is also possible
     * to overwrite changes made and committed by another transaction
     * after the current transaction read the item from the cache. This
     * problem would be caught by the update-time version-checking, if
     * the data is versioned or timestamped.
     */
    public Object get(Object key, long txTimestamp) throws CacheException
    {

        log.trace("Cache lookup: {}", key);

        Lockable lockable = (Lockable) cache.get(key);

        boolean gettable = lockable != null && lockable.isGettable(txTimestamp);

        if (gettable)
        {
            log.trace("Cache hit: {}", key);
            return ((Item) lockable).getValue();
        }
        else
        {
            if (log.isTraceEnabled())
            {
                if (lockable == null)
                {
                    log.trace("Cache miss: " + key);
                }
                else
                {
                    log.trace("Cached item was locked: " + key);
                }
            }
            return null;
        }
    }

    /**
     * Stop any other transactions reading or writing this item to/from
     * the cache. Send them straight to the database instead. (The lock
     * does time out eventually.) This implementation tracks concurrent
     * locks my transactions which simultaneously attempt to write to an
     * item.
     */
    public SoftLock lock(Object key, final Object version) throws CacheException
    {
        log.trace("Invalidating: {}", key);

        CacheEntryProcessor<Object, Lockable, SoftLock> processor = entry -> {
            Lockable lockable = entry.getValue();
            long timeout = cache.nextTimestamp() + cache.getTimeout();
            final Lock lock = lockable == null ?
                new Lock(timeout, nextLockId(), version) :
                lockable.lock(timeout, nextLockId());
            entry.setValue(lock);
            return lock;
        };

        return cache.invoke(key, processor);

    }

    /**
     * Do not add an item to the cache unless the current transaction
     * timestamp is later than the timestamp at which the item was
     * invalidated. (Otherwise, a stale item might be re-added if the
     * database is operating in repeatable read isolation mode.)
     */
    public boolean put(Object key, final Object value, final long txTimestamp, final Object version, final Comparator versionComparator)
        throws CacheException
    {

        // Only try to cache the object if there is no entry for that key in the
        // cache yet. Updates are handled by afterInsert() and afterUpdate().
        // The put is only responsible for initializing the cache entry for that key.
        if (cache.containsKey(key))
        {
            log.trace("Not overwriting already cached entry: {}", key);
            return false;
        }

        log.trace("Caching: {}", key);

        CacheEntryProcessor<Object, Lockable, Boolean> processor = entry -> {
            Lockable lockable = entry.getValue();

            boolean puttable = lockable == null ||
                lockable.isPuttable(txTimestamp, version, versionComparator);

            if (puttable)
            {
                entry.setValue(new Item(value, cache.nextTimestamp(), version));
                log.trace("Cached: {}", entry.getKey());
                return true;
            }
            else
            {
                if (log.isTraceEnabled())
                {
                    if (lockable.isLock())
                    {
                        log.trace("Item was locked: " + entry.getKey());
                    }
                    else
                    {
                        log.trace("Item was already cached: " + entry.getKey());
                    }
                }
                return false;
            }
        };

        return cache.invoke(key, processor) == Boolean.TRUE; // guard against possible null Boolean
    }

    /**
     * decrement a lock and put it back in the cache
     */
    private void decrementLock(CacheEntry entry, Lock lock)
    {
        //decrement the lock
        lock.unlock(cache.nextTimestamp());
        entry.setValue(lock);
    }

    /**
     * Release the soft lock on the item. Other transactions may now
     * re-cache the item (assuming that no other transaction holds a
     * simultaneous lock).
     */
    public void release(Object key, final SoftLock clientLock) throws CacheException
    {
        log.trace("Releasing: {}", key);

        CacheEntryProcessor<Object, Lockable, Void> processor = entry -> {
            final Lockable lockable = entry.getValue();
            if (isLockUpToDate(clientLock, lockable))
            {
                decrementLock(entry, (Lock) lockable);
            }
            else
            {
                log.warn("Lock has expired for key [{}] of [{}] before lock release could be performed", new Object[] {
                        entry.getKey(), lockable == null ? null : lockable.getClass()
                });
                clearLock(entry);
            }
            return null;
        };

        cache.invoke(key, processor);
    }

    void clearLock(CacheEntry entry)
    {
        long ts = cache.nextTimestamp() + cache.getTimeout();
        // create new lock that times out immediately
        Lock lock = new Lock(ts, nextLockId(), null);
        lock.unlock(ts);
        entry.setValue(lock);
    }

    public void clear() throws CacheException
    {
        cache.clear();
    }

    public void remove(Object key) throws CacheException
    {
        cache.remove(key);
    }

    public void destroy()
    {
        try
        {
            cache.destroy();
        }
        catch (Exception e)
        {
            log.warn("could not destroy cache", e);
        }
    }

    /**
     * Re-cache the updated state, if and only if there there are
     * no other concurrent soft locks. Release our lock.
     */
    public void afterUpdate(final Object key, final Object value, final Object version, final SoftLock clientLock)
        throws CacheException
    {

        log.trace("Updating: {}", key);

        CacheEntryProcessor<Object, Lockable, Void> processor = entry -> {
            final Lockable lockable = entry.getValue();
            if (isLockUpToDate(clientLock, lockable))
            {
                Lock lock = (Lock) lockable;
                if (lock.wasLockedConcurrently())
                {
                    // just decrement the lock, don't recache
                    // (we don't know which transaction won)
                    decrementLock(entry, lock);
                }
                else
                {
                    //recache the updated state
                    entry.setValue(new Item(value, cache.nextTimestamp(), version));
                    log.trace("Updated: {}", entry.getKey());
                }
            }
            else
            {
                log.warn("Lock has expired for key [{}] of [{}] version [{}] before afterUpdate could be performed", new Object[] {
                        entry.getKey(), lockable == null ? null : lockable.getClass(), version
                });
                clearLock(entry);
            }
            return null;
        };

        cache.invoke(key, processor);
    }

    /**
     * Add the new item to the cache, checking that no other transaction has
     * accessed the item.
     */
    public void afterInsert(Object key, final Object value, final Object version) throws CacheException
    {
        log.trace("Inserting: {}", key);

        CacheEntryProcessor<Object, Lockable, Void> processor = entry -> {
            Lockable lockable = entry.getValue();
            if (lockable == null)
            {
                entry.setValue(new Item(value, cache.nextTimestamp(), version));
                log.trace("Inserted: {}", entry.getKey());
            }
            return null;
        };

        cache.invoke(key, processor);
    }

    /**
     * Do nothing.
     */
    public void evict(Object key) throws CacheException
    {
        // noop
    }

    /**
     * Do nothing.
     */
    public void insert(Object key, Object value) throws CacheException
    {
        // noop
    }

    /**
     * Do nothing.
     */
    public void update(Object key, Object value) throws CacheException
    {
        // noop
    }

    /**
     * Is the client's lock commensurate with the item in the cache?
     * If it is not, we know that the cache expired the original
     * lock.
     */
    private static boolean isLockUpToDate(SoftLock clientLock, Lockable myLock)
    {
        //null clientLock is remotely possible but will never happen in practice
        return myLock != null &&
            myLock.isLock() &&
            clientLock != null &&
            ((Lock) clientLock).getId() == ((Lock) myLock).getId();
    }


    public static interface Lockable
    {
        public Lock lock(long timeout, int id);

        public boolean isLock();

        public boolean isGettable(long txTimestamp);

        public boolean isPuttable(long txTimestamp, Object newVersion, Comparator comparator);
    }

    /**
     * An item of cached data, timestamped with the time it was cached,.
     *
     * @see CoherenceCacheStrategy
     */
    public static final class Item implements Serializable, Lockable
    {

        private final long freshTimestamp;
        private final Object value;
        private final Object version;

        public Item(Object value, long currentTimestamp, Object version)
        {
            this.value = value;
            freshTimestamp = currentTimestamp;
            this.version = version;
        }

        /**
         * The timestamp on the cached data
         */
        public long getFreshTimestamp()
        {
            return freshTimestamp;
        }

        /**
         * The actual cached data
         */
        public Object getValue()
        {
            return value;
        }

        /**
         * Lock the item
         */
        public Lock lock(long timeout, int id)
        {
            return new Lock(timeout, id, version);
        }

        /**
         * Not a lock!
         */
        public boolean isLock()
        {
            return false;
        }

        /**
         * Is this item visible to the timestamped
         * transaction?
         */
        public boolean isGettable(long txTimestamp)
        {
            return freshTimestamp < txTimestamp;
        }

        /**
         * Don't overwite already cached items
         */
        public boolean isPuttable(long txTimestamp, Object newVersion, Comparator comparator)
        {
            // we really could refresh the item if it
            // is not a lock, but it might be slower
            //return freshTimestamp < txTimestamp
            return version != null && comparator.compare(version, newVersion) < 0;
        }

        public String toString()
        {
            return "Item[version=" + version +
                ",freshTimestamp=" + freshTimestamp + "]";
        }
    }

    /**
     * A soft lock which supports concurrent locking,
     * timestamped with the time it was released
     *
     * @author Gavin King
     */
    public static final class Lock implements Serializable, Lockable, SoftLock
    {
        private long unlockTimestamp = -1;
        private int multiplicity = 1;
        private boolean concurrentLock = false;
        private long timeout;
        private final int id;
        private final Object version;

        public Lock(long timeout, int id, Object version)
        {
            this.timeout = timeout;
            this.id = id;
            this.version = version;
        }

        public long getUnlockTimestamp()
        {
            return unlockTimestamp;
        }

        /**
         * Increment the lock, setting the
         * new lock timeout
         */
        public Lock lock(long timeout, int id)
        {
            concurrentLock = true;
            multiplicity++;
            this.timeout = timeout;
            return this;
        }

        /**
         * Decrement the lock, setting the unlock
         * timestamp if now unlocked
         *
         * @param currentTimestamp
         */
        public void unlock(long currentTimestamp)
        {
            if (--multiplicity == 0)
            {
                unlockTimestamp = currentTimestamp;
            }
        }

        /**
         * Can the timestamped transaction re-cache this
         * locked item now?
         */
        public boolean isPuttable(long txTimestamp, Object newVersion, Comparator comparator)
        {
            if (timeout < txTimestamp) return true;
            if (multiplicity > 0) return false;
            return version == null ?
                unlockTimestamp < txTimestamp :
                comparator.compare(version, newVersion) < 0; //by requiring <, we rely on lock timeout in the case of an unsuccessful update!
        }

        /**
         * Was this lock held concurrently by multiple
         * transactions?
         */
        public boolean wasLockedConcurrently()
        {
            return concurrentLock;
        }

        /**
         * Yes, this is a lock
         */
        public boolean isLock()
        {
            return true;
        }

        /**
         * locks are not returned to the client!
         */
        public boolean isGettable(long txTimestamp)
        {
            return false;
        }

        public int getId()
        {
            return id;
        }

        public String toString()
        {
            return "Lock[id=" + id +
                ",version=" + version +
                ",multiplicity=" + multiplicity +
                ",unlockTimestamp=" + unlockTimestamp + "]";
        }

    }


    public void setMinimalPuts(boolean minimalPuts) throws HibernateException
    {
        if (minimalPuts) throw new HibernateException("minimal puts not supported for read-write cache");
    }

}






