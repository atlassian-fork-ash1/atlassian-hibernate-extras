package com.atlassian.hibernate.extras.tangosol;

public interface CacheEntryProcessor<K, V, T>
{
    T process(CacheEntry<K,V> input);
}
