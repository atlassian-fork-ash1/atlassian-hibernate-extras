package com.atlassian.hibernate.extras;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;

public interface ExportHibernateHandle
{
    public Object get(Session session) throws HibernateException;
    public Class getClazz();
}
